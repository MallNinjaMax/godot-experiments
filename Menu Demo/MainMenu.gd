extends Control

func _ready():
	for button in get_children():
		if button is Button:
			button.connect("pressed", self, "_on_Button", [button.text])

func _on_Button(name):
	get_parent().change_menu(name)
