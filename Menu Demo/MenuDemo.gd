extends Control

var menus = []


func _ready():
	get_menus()


func get_menus():
	for menu in get_children():
		if not menu.name == "Main Menu":
			menus.append(menu)
			menu.hide()


func change_menu(menu_name):
	for menu in menus:
		if menu.name != menu_name:
			menu.hide()
		else:
			menu.show()
