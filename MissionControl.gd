extends Control


var minion_roster = []
var availible_missions = []
var selected_minion
var selected_mission


func _ready():
	get_minions()


func get_minions():
	
	pass


func _on_Attempt_pressed():
	if selected_minion and selected_mission:
		var successes = 0
		var blood_mod = 0
		
		if selected_minion["blood"] > 0: 
			blood_mod = 1
		
		randomize()
		var roll1 = (randi() % 6) + 1
		var roll2 = (randi() % 6) + 1
		
		if (selected_minion["strength"] + blood_mod) + roll1 > selected_mission["difficulty"]:
			successes += 1
		
		if (selected_minion["stamina"] + blood_mod) + roll2 > selected_mission["difficulty"]:
			successes += 1
		
		if successes > 1:
			print("Success")


func select_minion(var minion):
	selected_minion = minion_roster[minion]


func select_mission(var mission):
	selected_mission = availible_missions[mission]


func add_minion(var nme, var sth, var stm, var bld):
	var new_minion = {"name": nme, "strength": sth, "stamina": stm, "blood": bld}
	minion_roster.append(new_minion)


func add_mission(var nme, var dif):
	var new_mission = {"name": nme, "difficulty": dif}
	availible_missions.append(new_mission)


# STUPID STUFF BELOW


func generate_minions():
	var presets = [
		["Weak", 1, 1, 0],
		["Average", 3, 3, 2],
		["Strong", 5, 5, 7]
		]
	
	for i in range(0, presets.size()):
		var entry = presets[i]
		add_minion(entry[0], entry[1], entry[2], entry[3])

func generate_missions():
	var presets = [
		["Easy", 3],
		["Normal", 6],
		["Hard", 9]
		]
	
	for i in range(0, presets.size()):
		var entry = presets[i]
		add_mission(entry[0], entry[1])
